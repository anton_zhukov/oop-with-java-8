package com.dev.firsttask.serialization;

import com.dev.firsttask.entity.Vehicle;

import java.io.Serializable;

public class Driver implements Serializable {

    private static final long serialVersionUID = 1L;

    private String name;
    private int age;
    private Vehicle ownCar;
    private transient String nickName;

    public Driver(String name, int age, Vehicle ownCar, String nickOfDriver) {
        this.name = name;
        this.age = age;
        this.ownCar = ownCar;
        nickName = nickOfDriver;
    }

    @Override
    public String toString() {
        return "Driver{" +
                "name = " + name
                + ", age = " + age
                + ", nick-name = " + nickName + "} \n"
                + ownCar.toString();
    }
}