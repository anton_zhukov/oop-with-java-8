package com.dev.firsttask.serialization;

import com.dev.firsttask.exception.RestoreInstanceException;
import org.apache.log4j.Logger;

import java.io.*;


public class Serializator {

    private static final Logger LOG = Logger.getLogger(Serializator.class);

    public boolean serialization(Driver d, String fileName) {
        boolean flag = false;
        File outputFile = new File(fileName);
        ObjectOutputStream outputStream = null;
        try {
            FileOutputStream fos = new FileOutputStream(outputFile);
            outputStream = new ObjectOutputStream(fos);
            outputStream.writeObject(d);
            flag = true;
        } catch (FileNotFoundException e) {
            LOG.error("File can't be created: " + e);
        } catch (NotSerializableException e) {
            LOG.error("Class don't support serialization " + e);
        } catch (IOException e) {
            LOG.error(e);
        } finally {
            try {
                if (outputStream != null) {
                    outputStream.close();
                }
            } catch (IOException e) {
                LOG.error("Stream closing error");
            }
        }
        return flag;
    }

    public Driver deserialization(String fileName) throws RestoreInstanceException {
        File inputFile = new File(fileName);
        ObjectInputStream inputStream = null;
        try {
            FileInputStream fis = new FileInputStream(inputFile);
            inputStream = new ObjectInputStream(fis);
            return (Driver) inputStream.readObject();
        } catch (ClassNotFoundException ce) {
            LOG.error("Class doesn't exist: " + ce);
        } catch (FileNotFoundException e) {
            LOG.error("File doesn't exist: ", e);
        } catch (InvalidClassException ioe) {
            LOG.error("SerialVersionUID of classes don't match: ", ioe);
        } catch (IOException ioe) {
            LOG.error("IOException: " + ioe);
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException e) {
                LOG.error("Stream closing error ", e);
            }
        }
        throw new RestoreInstanceException("Instance isn't restored");
    }
}

