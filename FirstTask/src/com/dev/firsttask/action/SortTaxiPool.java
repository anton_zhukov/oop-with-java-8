package com.dev.firsttask.action;

import com.dev.firsttask.entity.TaxiPool;

import java.util.Collections;

public class SortTaxiPool {

    /**
     * Method for sorting by Fuel consumption
     *
     * @param pool the reference object for getting list of {@code Taxi} instances
     */
    public static void sortPool(TaxiPool pool) {
        Collections.sort(pool.getPool());
    }


}
