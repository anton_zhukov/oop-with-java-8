package com.dev.firsttask.action;

import com.dev.firsttask.entity.Taxi;
import com.dev.firsttask.entity.TaxiPool;

import java.util.ArrayList;
import java.util.List;

public class SearchVehicle {

    /**
     * Method for searching by Carrying Ability. It was created for serving method {@code searchByCapAndCarryAb}
     *
     * @param poolOne  the reference object for getting list of Taxi instances
     * @param minCarry Minimum range of Carrying Ability for searching
     * @param maxCarry Maximum range of Carrying Ability for searching
     * @return Back list of Taxis that are appropriate to search conditions
     */
    public static List<Taxi> searchByCarryingAbility(
            TaxiPool poolOne, double minCarry, double maxCarry) {

        List<Taxi> searchResult = new ArrayList<Taxi>();
        for (Taxi v : poolOne.getPool()) {
            if (v.getCarryingAbility() >= minCarry && v.getCarryingAbility() <= maxCarry) {
                searchResult.add(v);
            }
        }
        return searchResult;
    }

    /**
     * Method for searching by Capacity And Carrying Ability
     *
     * @param poolTwo     the reference object for getting list of Taxi instances
     * @param minCapacity Minimum range of Capacity for searching
     * @param maxCapacity Maximum range of Capacity for searching
     * @param minCarry    Minimum range of Carrying Ability for searching
     * @param maxCarry    Maximum range of Carrying Ability for searching
     * @return Back list of Taxis that are appropriate to search conditions
     */
    public static List<Taxi> searchByCapAndCarryAb(
            TaxiPool poolTwo, int minCapacity, int maxCapacity, double minCarry, double maxCarry) {

        List<Taxi> searchResult = new ArrayList<Taxi>();
        List<Taxi> resultByCarryingAbility = searchByCarryingAbility(poolTwo, minCarry, maxCarry);
        for (Taxi vehicle : resultByCarryingAbility) {
            if (vehicle.getCapacity() >= minCapacity && vehicle.getCapacity() <= maxCapacity) {
                searchResult.add(vehicle);
            }
        }
        return searchResult;
    }
}