package com.dev.firsttask.action;

import com.dev.firsttask.entity.Taxi;
import com.dev.firsttask.entity.TaxiPool;
import com.dev.firsttask.exception.CountCostException;
import org.apache.log4j.Logger;

public class CountCost {

    private static final Logger LOG = Logger.getLogger(CountCost.class);

    /**
     * @param pool         the reference object for getting list of Taxi instances
     * @param coefInflation Coefficient of inflation
     * @return if parameters are correct, we will gain cost of taxi-pool adjusted for inflation
     * @throws CountCostException if parameter {@code coefInflation} is not correct, that kind of exception will be thrown
     */
    public static int countCostPool(TaxiPool pool, double coefInflation) throws CountCostException {
        int result = 0;

        if (coefInflation < 1) {
            throw new CountCostException("Coefficient of inflation = " + coefInflation + " is less than 1.0");
        }

        LOG.info("Argument coefInflation is " + coefInflation);
        for (Taxi veh : pool.getPool()) {
            result += veh.getCost();
        }
        return (int) (result * coefInflation);
    }
}