package com.dev.firsttask.main;

import com.dev.firsttask.function.Demonstration;

public class Runner {

    public static void main(String[] args) {
        Demonstration.demonstrate();
    }
}