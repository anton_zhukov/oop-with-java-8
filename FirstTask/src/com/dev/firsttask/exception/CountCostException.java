package com.dev.firsttask.exception;

public class CountCostException extends Exception {

    public CountCostException(String message) {
        super(message);
    }

    public CountCostException(String message, Throwable cause) {
        super(message, cause);
    }

    public CountCostException(Throwable cause) {
        super(cause);
    }
}
