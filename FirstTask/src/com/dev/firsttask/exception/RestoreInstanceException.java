package com.dev.firsttask.exception;


public class RestoreInstanceException extends Exception {
    public RestoreInstanceException() {
    }

    public RestoreInstanceException(String message) {
        super(message);
    }

    public RestoreInstanceException(String message, Throwable cause) {
        super(message, cause);
    }

    public RestoreInstanceException(Throwable cause) {
        super(cause);
    }
}
