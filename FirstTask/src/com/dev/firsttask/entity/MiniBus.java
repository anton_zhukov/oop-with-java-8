package com.dev.firsttask.entity;


import com.dev.firsttask.exception.WrongValueException;

public class MiniBus extends Taxi {
    private String idCashRegister;
    public static final int ID_LENGTH = 8;

    public MiniBus(int cost, double consumptionFuel, double carryingAbility, int capacity,
                   String registerNumber, TaxiService taxiServiceName, String idCashRegister) {
        super(cost, consumptionFuel, carryingAbility, capacity, registerNumber, taxiServiceName);
        this.idCashRegister = idCashRegister;
    }

    public String getIdCashRegister() {
        return idCashRegister;
    }

    public void setIdCashRegister(String idCashRegister) throws WrongValueException {
        if (idCashRegister.length() == ID_LENGTH) {
            this.idCashRegister = idCashRegister;
        } else {
            throw new WrongValueException("Wrong length of idCashRegister");
        }
    }

    @Override
    public String toString() {
        return "MiniBus" +
                "\nregistrationNumber" + super.getRegistrationNumber() +
                "\nidCashRegister='" + idCashRegister + '\'' +
                "\n{" + super.getTaxiServiceName() + "}" +
                "\ncost=" + super.getCost() +
                "\nconsumptionFuel=" + super.getConsumptionFuel() +
                "\ncarryingAbility=" + super.getCarryingAbility() +
                "\ncapacity=" + super.getCapacity() + "\n";
    }
}
