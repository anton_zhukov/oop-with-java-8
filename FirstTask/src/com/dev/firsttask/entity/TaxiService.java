package com.dev.firsttask.entity;

public enum TaxiService {
    ALMAZ(7788), STOLITSA(135), ELIT(152), CHRYSTAL(7778), TAXI_CARGO(163);
    private int phoneNumber;

    TaxiService(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return "TaxiService{" + name() +
                ": phoneNumber = " + phoneNumber +
                "}";
    }
}
