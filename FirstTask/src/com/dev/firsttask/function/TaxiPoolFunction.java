package com.dev.firsttask.function;

import com.dev.firsttask.entity.Taxi;
import com.dev.firsttask.entity.TaxiPool;
import com.dev.firsttask.entity.Vehicle;
import com.dev.firsttask.exception.CountCostException;
import com.dev.firsttask.exception.VehicleNotFoundException;

import java.util.Collections;
import java.util.Comparator;

public class TaxiPoolFunction {
    public static Taxi searchByCost(TaxiPool pool, int cost) throws VehicleNotFoundException {
        return pool.getPool().stream()
                .filter(t -> t.getCost() == cost)
                .findFirst()
                .orElseThrow(() -> new VehicleNotFoundException());
    }

    /**
     * Method for sorting by Carrying Ability
     *
     * @param pool the reference object for getting list of {@code Taxi} instances
     */
    public static void sortByCarryingAbility(TaxiPool pool) {
        Collections.sort(pool.getPool(), Comparator.comparingDouble(Vehicle::getCarryingAbility));
    }

    /**
     * Method for sorting by Registration Number
     *
     * @param pool the reference object for getting list of {@code Taxi} instances
     */
    public static void sortByRegistrationNumber(TaxiPool pool) {
        Collections.sort(pool.getPool(), Comparator.comparing(Vehicle::getRegistrationNumber));
    }

    /**
     * Method for counting cost of Vehicles with person capacity more than five
     *
     * @param pool          the reference object for getting list of Taxi instances
     * @param coefInflation Coefficient of inflation
     * @return if parameters are correct, we will gain cost of MiniBuses adjusted for inflation
     * @throws com.dev.firsttask.exception.CountCostException if parameter {@code coefInflation} is not correct,
     *                                                        that kind of exception will be thrown
     */
    public static int countCostVehicles(TaxiPool pool, double coefInflation, int capacityRange) throws CountCostException {
        int result;
        if (coefInflation < 1) {
            throw new CountCostException("Coefficient of inflation = " + coefInflation + " is less than 1.0");
        }
        result = pool.getPool().stream()
                .filter(v -> v.getCapacity() > capacityRange)
                .mapToInt(Vehicle::getCost)
                .sum();
        return (int) (result * coefInflation);
    }
}
