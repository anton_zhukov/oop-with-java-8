package com.dev.firsttask.function;

import com.dev.firsttask.action.CountCost;
import com.dev.firsttask.action.SearchVehicle;
import com.dev.firsttask.action.SortTaxiPool;
import com.dev.firsttask.creation.CreatingTaxiPool;
import com.dev.firsttask.entity.Taxi;
import com.dev.firsttask.entity.TaxiPool;
import com.dev.firsttask.entity.Vehicle;
import com.dev.firsttask.exception.CountCostException;
import com.dev.firsttask.exception.RestoreInstanceException;
import com.dev.firsttask.exception.VehicleNotFoundException;
import com.dev.firsttask.main.Runner;
import com.dev.firsttask.serialization.Driver;
import com.dev.firsttask.serialization.Serializator;
import org.apache.log4j.Logger;

import java.io.InvalidObjectException;
import java.util.List;


public class Demonstration {
    private static final Logger LOG = Logger.getLogger(Runner.class);

    public static void demonstrate() {
        // Creating TaxiPool
        TaxiPool taxiPool = new TaxiPool(CreatingTaxiPool.createTaxiPool());
        LOG.info("Taxi-pool is created");

        //Sorting TaxiPool by Consumption of Fuel
        SortTaxiPool.sortPool(taxiPool);
        LOG.info("Taxi-pool is sorted by Consumption of Fuel:");

        taxiPool.getPool().stream().forEach(vehicle -> LOG.debug("Fuel: " + vehicle.getConsumptionFuel() + "L per 100km"
                + ", Reg. number: " + vehicle.getRegistrationNumber()
                + ", " + vehicle.getTaxiServiceName()
                + ", Carrying ability: " + vehicle.getCarryingAbility() + " kg"));


        //Sorting TaxiPool by Carrying Ability
        TaxiPoolFunction.sortByCarryingAbility(taxiPool);
        LOG.info("Taxi-pool is sorted by Carrying Ability:");

        taxiPool.getPool().stream().forEach((vehicle) -> LOG.debug("Carrying ability: " + vehicle.getCarryingAbility()
                + " kg, Reg. number: " + vehicle.getRegistrationNumber()
                + ", " + vehicle.getTaxiServiceName()
                + ", Fuel: " + vehicle.getConsumptionFuel() + "L per 100km"));


        //Sorting TaxiPool by Registration Number
        TaxiPoolFunction.sortByRegistrationNumber(taxiPool);
        LOG.info("Taxi-pool is sorted by Registration Number:");

        taxiPool.getPool().stream().forEach((vehicle) -> LOG.debug("Reg. number: " + vehicle.getRegistrationNumber()
                + ", " + vehicle.getTaxiServiceName()
                + ", Carrying ability: " + vehicle.getCarryingAbility()
                + " kg, Fuel: " + vehicle.getConsumptionFuel() + "L per 100km"));


        // Counting cost of Taxi-Pool
        final double INFLATION = 1.2;
        try {
            LOG.info("Taxi-pool costs " + CountCost.countCostPool(taxiPool, INFLATION) + "$ adjusted for inflation");
        } catch (CountCostException e) {
            LOG.error("Wrong value of inflation coefficient");
        }

        // Counting cost of Vehicles with person capacity more than five
        final int CAPACITY_RANGE = 5;
        try {
            LOG.info("Vehicle with person capacity more than five cost " +
                    TaxiPoolFunction.countCostVehicles(taxiPool, INFLATION, CAPACITY_RANGE) + "$ adjusted for inflation");
        } catch (CountCostException e) {
            LOG.error("Wrong value of inflation coefficient");
        }

        // Ranges for searching
        final int MIN_CAPACITY = 5;
        final int MAX_CAPACITY = 12;
        final double MIN_CARRY = 700;
        final double MAX_CARRY = 3100;

        // Searching Vehicles by Capacity And CarryingAbility
        List<Taxi> result = SearchVehicle.searchByCapAndCarryAb(taxiPool, MIN_CAPACITY, MAX_CAPACITY, MIN_CARRY, MAX_CARRY);
        LOG.info("Searching by Capacity And Carrying Ability is done.");
        result.stream().forEach(vehicle -> LOG.debug("Reg. number: " + vehicle.getRegistrationNumber() +
                ", " + vehicle.getTaxiServiceName() +
                ", Capacity: " + vehicle.getCapacity() + " persons, Carrying ability: " +
                vehicle.getCarryingAbility() + " kg"));


        // Searching Taxi by Cost
        final int COST = 21550;
        Taxi resultByCost = null;
        try {
            resultByCost = TaxiPoolFunction.searchByCost(taxiPool, COST);
        } catch (VehicleNotFoundException e) {
            LOG.error("VehicleNotFoundException!", e);
        }
        LOG.debug("Searching by Cost result: " + resultByCost.getClass().getSimpleName() + ", "
                + resultByCost.getCost() + "$, " + resultByCost.getTaxiServiceName() + ", "
                + resultByCost.getRegistrationNumber());


        //Serialization
        Vehicle ownCar = new Vehicle(10950, 9.2, 560, 5, "6254 AC-7");
        Driver driver = new Driver("Oleg Shatov", 34, ownCar, "Speedster");
        LOG.debug(driver);
        String file = "data\\demo.data";
        Serializator serializator = new Serializator();
        if (serializator.serialization(driver, file)) {
            LOG.info("Object has been serialized successfully");
        }

        Driver driverFromFile = null;
        try {
            driverFromFile = serializator.deserialization(file);
        } catch (RestoreInstanceException e) {
            LOG.error("InvalidObjectException", e);
        }
        LOG.debug(driverFromFile);
    }
}

